//
//  MenuTableViewController.m
//  CoffeePod
//
//  Created by Jaime Moises Gutierrez on 9/23/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import "MenuTableViewController.h"
#import "Connectivity.h"

#define kMenuAPIURL @"http://www.inphomercial.com/coffeepod/menu.php"

@interface MenuTableViewController ()

@property (nonatomic,strong) NSString *descriptionString;
@end

@implementation MenuTableViewController

- (void)viewWillAppear:(BOOL)animated
{
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(loadMenuData) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    if ([self.menuArray containsObject:@"desc"])
    {
        self.descriptionString = [NSString stringWithString:[self.JSONData objectForKey:[self.menuArray objectAtIndex:[self.menuArray indexOfObject:@"desc"]]]];
    }
    
    [self loadMenuData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.tableFooterView = [[UIView alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

#pragma mark - Table view data source

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] init];
    if (self.descriptionString != nil)
    {
        // create a custom multi-line label
        label.text = self.descriptionString;
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentCenter;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        //        label.font = self.headerFont;
        [label sizeToFit];
        [self removeDescriptionFromMenuArray];
        [self.tableView reloadData];
    }
    
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.descriptionString != nil)
    {
        // compute size
        NSString *text = self.descriptionString;
        CGRect rect = [text boundingRectWithSize:CGSizeMake(CGRectGetWidth(tableView.frame), CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:nil context:nil];
        
        return MAX(CGRectGetHeight(rect), 44); // keep height no smaller than Plain cells
    }
    else
    {
        return 0.0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuArray.count;
}

- (void)reloadTableView
{
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MenuCell"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"MenuCell"];
        cell.textLabel.adjustsFontSizeToFitWidth = YES;
        cell.textLabel.minimumScaleFactor = 0.5;
        cell.detailTextLabel.minimumScaleFactor = 0.5;
        cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if ([[self.menuArray objectAtIndex:indexPath.row] isEqualToString:@"desc"])
    {
        cell.textLabel.text = [self.JSONData objectForKey:[self.menuArray objectAtIndex:indexPath.row]];
    }
    else
    {
        cell.textLabel.text = [self.menuArray objectAtIndex:indexPath.row];
        if ([self objectIsNeitherArrayOrDictionaryType:[self.JSONData objectForKey:[self.menuArray objectAtIndex:indexPath.row]]])
        {
            cell.detailTextLabel.text = [NSString stringWithFormat:@"$%.2f",[[self.JSONData objectForKey:[self.menuArray objectAtIndex:indexPath.row]] floatValue]];
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    return cell;
}

- (BOOL)objectIsNeitherArrayOrDictionaryType:(id)object
{
    if ([object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *keyString = [self.menuArray objectAtIndex:indexPath.row];
    
    [self.JSONData enumerateKeysAndObjectsWithOptions:NSEnumerationConcurrent
                                             usingBlock:^(id key, id object, BOOL *stop)
    {
        if (key == keyString)
        {
            NSLog(@"found key");
            NSLog(@"key:%@",key);
            NSLog(@"object:%@",object);
         
            NSDictionary *dict =
            @{
              @"key":key,
              @"object":object,
              };
            [self performSelectorOnMainThread:@selector(enumarationDidFinish:) withObject:dict waitUntilDone:NO];
        }
    }];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    return view;
}

- (void)enumarationDidFinish:(NSDictionary *)info
{
    if ([[info objectForKey:@"object"] respondsToSelector:@selector(allKeys)])
    {
        MenuTableViewController *vc = [[MenuTableViewController alloc] init];
        vc.JSONData = [info objectForKey:@"object"];
        vc.menuArray = [NSMutableArray arrayWithArray:[vc.JSONData allKeys]];
        vc.title = [info objectForKey:@"key"];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (BOOL)reachedBottomOfDictionary:(id)object
{
    if ([[[object objectForKey:@"object"] allKeys] count] <= 1)
    {
        NSLog(@"reached bottom of dictionary");
        return YES;
    }
    else
    {
        NSLog(@"Not the bottom of dictionary");
        return NO;
    }
}

- (BOOL)objectIsOfNumericType:(id)object
{
    if ([object isKindOfClass:[NSNumber class]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (BOOL)objectIsOfStringType:(id)object
{
    if ([object isKindOfClass:[NSString class]])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)showBadConnectionMessage
{
    [Connectivity showBadConnectionMessage];
    [self.refreshControl endRefreshing];
}

- (BOOL)currentViewIsRoot
{
    if (self == [self.navigationController.viewControllers objectAtIndex:0])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)loadMenuData
{
    if ([self currentViewIsRoot])
    {
        if ([Connectivity internetConnectionIsActive])
        {
            NSString *stringURL = [NSString stringWithFormat:kMenuAPIURL];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:stringURL]
                                                                   cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                               timeoutInterval:10];
            
            [request setHTTPMethod: @"GET"];
            
            NSOperationQueue *oq = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:request queue:oq completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
             {
                 //             NSLog(@"response:\n%@",[response description]);
                 //             NSLog(@"data:\n%@",[data description]);
                 //             NSLog(@"connection error:\n%@",[connectionError description]);
                 
                 if (data != nil)
                 {
                     self.JSONData = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                     //                 NSLog(@"json dictionary received: \n%@",self.JSONData);
                     
                     self.menuArray = [NSMutableArray arrayWithArray:[self.JSONData allKeys]];
                     //                 NSLog(@"json array received: \n%@",self.menuArray);
                     
                     [self performSelectorOnMainThread:@selector(reloadTableView) withObject:nil waitUntilDone:YES];
                 }
                 else
                 {
                     [self performSelectorOnMainThread:@selector(showBadConnectionMessage) withObject:nil waitUntilDone:YES];
                 }
             }];
        }
    }
    else
    {
        [self.refreshControl endRefreshing];
    }
}

- (void)removeDescriptionFromMenuArray
{
    if ([self.menuArray containsObject:@"desc"])
    {
        NSLog(@"menu array: \n%@",[self.menuArray description]);
        [self.menuArray removeObjectAtIndex:[self.menuArray indexOfObject:@"desc"]];
        NSLog(@"menu array: \n%@",[self.menuArray description]);
    }
}

@end
