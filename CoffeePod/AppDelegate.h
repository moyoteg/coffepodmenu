//
//  AppDelegate.h
//  CoffeePod
//
//  Created by Jaime Moises Gutierrez on 9/23/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

