//
//  Connectivity.m
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 9/14/14.
//
//

#import "Connectivity.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <UIKit/UIKit.h>

@implementation Connectivity

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

+ (BOOL)internetConnectionIsActive
{
    Connectivity *connectivity = [[Connectivity alloc] init];
    if ([connectivity connected])
    {
        NSLog(@"active connection");
        return true;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No active connection" message:@"Please connect to the internet!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        NSLog(@"No active connections");
        return false;
    }
    
}

+ (void)showBadConnectionMessage
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Time out" message:@"Please make sure you have a reliable connection and try again" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
@end
