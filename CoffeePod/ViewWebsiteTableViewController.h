//
//  ViewWebsiteTableViewController.h
//  CoffeePod
//
//  Created by Jaime Moises Gutierrez on 9/26/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewWebsiteTableViewController : UITableViewController

@end
