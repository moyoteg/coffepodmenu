//
//  ViewWebsiteTableViewController.m
//  CoffeePod
//
//  Created by Jaime Moises Gutierrez on 9/26/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import "ViewWebsiteTableViewController.h"

#define kWebsiteURL @"http://inphomercial.com/coffeepod/"

@interface ViewWebsiteTableViewController () <UIWebViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation ViewWebsiteTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.webView.delegate = self;
    self.webView.scrollView.bounces = NO;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kWebsiteURL]cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10.0]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.activityIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityIndicator stopAnimating];
}

@end
