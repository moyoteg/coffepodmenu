//
//  MenuTableViewController.h
//  CoffeePod
//
//  Created by Jaime Moises Gutierrez on 9/23/14.
//  Copyright (c) 2014 Jaime Moises Gutierrez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewController : UITableViewController
{
}

@property (nonatomic, strong) NSMutableArray *menuArray;
@property (nonatomic, strong) NSDictionary *JSONData;

@end
